package com.processmaker.pmautomation;

import com.processmaker.pmautomation.common.Logger;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;


/**
 * Test suite main executor
 */
@RunWith(value = Suite.class)
@SuiteClasses( value = {
    App.class
})

public class TestSuiteAll
{
    @BeforeClass
    public static void setUpClass() {
        Logger.addLog("Master setup");
    }

    @AfterClass
    public static void tearDownClass() {

    }
}
