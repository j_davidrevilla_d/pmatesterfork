package com.processmaker.pmautomation.common;

public enum BrowserType {
    FIREFOX,
    CHROME,
    IE
}
