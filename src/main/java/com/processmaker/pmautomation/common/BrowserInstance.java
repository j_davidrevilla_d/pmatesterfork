package com.processmaker.pmautomation.common;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class BrowserInstance {
    private JavascriptExecutor _javascriptExecutor = null;
    private WebDriver _instanceDriver = null;
    private int _implicitWaitSeconds = 0;
    private String _browserMode, _browserName, _browserVersion, _browserPlatform, _remoteServerUrl;
    private BrowserConfiguration _browserConfiguration;

    /**
     * Read the browser configuration from configuration file in base to specified browser configuration
     * @param browserConfiguration the name of the browser configuration to search the data.
     * @return BrowserSettings
     */
    public static BrowserSettings getBrowserSettings(String browserConfiguration) throws IOException {
        int browserConfigurations = 1;
        int browserIndex = 0;
        String auxBrowserName = "";
        BrowserSettings browserSettings = new BrowserSettings();

        if(browserConfiguration.equals("")){
            //default browser
            browserSettings.setBrowserName(ConfigurationSettings.getInstance().getSetting("browser.name"));
            browserSettings.setBrowserVersion(ConfigurationSettings.getInstance().getSetting("browser.version"));
            browserSettings.setBrowserPlatform(ConfigurationSettings.getInstance().getSetting("browser.platform"));
            browserSettings.setBrowserMode(ConfigurationSettings.getInstance().getSetting("browser.mode"));
            if(browserSettings.getBrowserMode().equals("remote")){
                browserSettings.setRemoteServerUrl(ConfigurationSettings.getInstance().getSetting("remote.server.url"));
            }
        }
        else{
            //get the number of specified browser configuration:
            //get configuration specific for the specified browser
            browserConfigurations = Integer.parseInt(ConfigurationSettings.getInstance().getSetting("browser.configurations"));
            for (int i = 1; i <= browserConfigurations; i++){
                auxBrowserName = ConfigurationSettings.getInstance().getSetting("browser.configuration" + i);
                if(auxBrowserName.equals(browserConfiguration)){
                    browserIndex = i;
                    break;
                }
            }

            browserSettings.setBrowserName(ConfigurationSettings.getInstance().getSetting("browser.name" + (browserIndex)));
            browserSettings.setBrowserVersion(ConfigurationSettings.getInstance().getSetting("browser.version"  + (browserIndex)));
            browserSettings.setBrowserPlatform(ConfigurationSettings.getInstance().getSetting("browser.platform"  + (browserIndex)));
            browserSettings.setBrowserMode(ConfigurationSettings.getInstance().getSetting("browser.mode" + (browserIndex)));
            if(browserSettings.getBrowserMode().equals("remote")){
                browserSettings.setRemoteServerUrl(ConfigurationSettings.getInstance().getSetting("remote.server.url" + (browserIndex)));
            }
        }

        return browserSettings;
    }

    /**
     * Constructor with browser settings as parameter
     * @param browserSettings
     * @throws Exception
     */
    public BrowserInstance(BrowserSettings browserSettings) throws Exception {
        this(browserSettings.getBrowserMode(), browserSettings.getBrowserName(),
                browserSettings.getBrowserVersion(), browserSettings.getBrowserPlatform(),
                browserSettings.getRemoteServerUrl());

    }

    /**
     * Constructor with all elements
     * @param browserMode local / remote
     * @param browserName chrome / ie / firefox
     * @param browserVersion
     * @param browserPlatform WINDOWS / LINUX / MAC
     * @param remoteServerUrl
     * @throws Exception
     */
    public BrowserInstance(String browserMode, String browserName, String browserVersion, String browserPlatform, String remoteServerUrl) throws Exception {
        //create a new instance of the Browser
        _browserMode = browserMode;
        _browserName = browserName;
        _browserVersion = browserVersion;
        _browserPlatform = browserPlatform;
        _remoteServerUrl = remoteServerUrl;

        if(browserMode.equals("local")){
            System.out.printf("Instance local browser:%s, version:%s, platform:%s, url:%s \n",
                    browserName, browserVersion, browserPlatform, remoteServerUrl);
            if(browserName.equals("chrome")){
                _instanceDriver = new ChromeDriver();
            }
            if(browserName.equals("ie")){
                _instanceDriver = new InternetExplorerDriver();
            }
            if(browserName.equals("firefox")){

                FirefoxProfile profile = new FirefoxProfile();
                profile.setPreference("browser.download.dir", Utils.getDownloadWorkingDirectory(this));
                profile.setPreference("browser.download.folderList", 2);
                profile.setPreference("browser.helperApps.neverAsk.saveToDisk", "application/x-xpinstall;application/x-zip;application/x-zip-compressed;application/octet-stream;application/zip;application/pdf;application/msword;text/plain;application/octet");

                _instanceDriver = new FirefoxDriver();
            }
        }

        if(browserMode.equals("remote")){

            System.out.printf("Instance Remote browser:%s, version:%s, platform:%s, url:%s \n",
                    browserName, browserVersion, browserPlatform, remoteServerUrl);

            DesiredCapabilities desiredCapabilities = new DesiredCapabilities();

            if(browserName.equals("chrome")){
                desiredCapabilities = DesiredCapabilities.chrome();
            }else if(browserName.equals("ie")){
                desiredCapabilities = DesiredCapabilities.internetExplorer();
            }else if(browserName.equals("firefox")){
                FirefoxProfile profile = new FirefoxProfile();
                profile.setPreference("browser.helperApps.neverAsk.saveToDisk", "application/x-xpinstall;application/x-zip;application/x-zip-compressed;application/octet-stream;application/zip;application/pdf;application/msword;text/plain;application/octet");
                desiredCapabilities = DesiredCapabilities.firefox();
                desiredCapabilities.setCapability("elementScrollBehavior", 1);
                desiredCapabilities.setCapability(FirefoxDriver.PROFILE, profile.toString());
            }

            if(browserVersion != null && !browserVersion.equals("")){
                desiredCapabilities.setVersion(browserVersion);
            }

            if(browserPlatform != null && !browserPlatform.equals("")){
                if(browserPlatform.equals("WINDOWS")){
                    desiredCapabilities.setPlatform(Platform.WINDOWS);
                }else if(browserPlatform.equals("LINUX")){
                    desiredCapabilities.setPlatform(Platform.LINUX);
                }else if(browserPlatform.equals("MAC")){
                    desiredCapabilities.setPlatform(Platform.MAC);
                }
            }else{
                desiredCapabilities.setPlatform(Platform.ANY);
            }

            Logger.addLog("Remote Browser capabilities browserName:" + desiredCapabilities.getBrowserName() +
                    " version:" + desiredCapabilities.getVersion() + " platform:" + desiredCapabilities.getPlatform());

            URL url=new URL(remoteServerUrl);

            _instanceDriver = new RemoteWebDriver(url, desiredCapabilities);
        }

        //maximize browser by default
        maximize();

        //get javascript executor
        //it's required
        if (_instanceDriver instanceof JavascriptExecutor) {
            _javascriptExecutor = (JavascriptExecutor)_instanceDriver;
        }else{
            throw  new Exception("Could not get javascript Executor from webdriver.");
        }
    }

    /**
     * Get the browser type
     * @return "FIREFOX", "CHROME", "IE"
     */
    public BrowserType getBrowserType(){
        switch (this._browserName){
            case "firefox":
                return BrowserType.FIREFOX;
            case "chrome":
                return BrowserType.CHROME;
            case "ie":
                return BrowserType.IE;
        }
        return null;
    }

    /**
     * Get Browser Platform
     * @return "Windows", "Linux, "Mac"
     */
    public String getBrowserPlatform(){
        return this._browserPlatform;
    }

    /**
     * Get web driver
     * @return WebDriver
     */
    public WebDriver getInstanceDriver(){
        return _instanceDriver;
    }

    /**
     * Navigate to url
     * @param url url to redirect
     */
    public void gotoUrl(String url){
        _instanceDriver.get(url);
    }

    /**
     * Get Title of page
     * @return Title of page
     */
    public String title(){
        return _instanceDriver.getTitle();
    }

    /**
     * Close the current window, quitting the browser if it's the last window currently open.
     */
    public void close(){
        _instanceDriver.close();
    }

    /**
     * Quits this driver, closing every associated window.
     */
    public void quit(){
        _instanceDriver.quit();
    }

    /**
     * Refresh web page
     */
    public void refresh(){
        _instanceDriver.navigate().refresh();
    }

    /**
     * Maximize browser
     */
    public void maximize(){

        _instanceDriver.manage().window().maximize();

    }

    /**
     * Selects either the first frame on the page, or the main document when a page contains iframes.
     */
    public void switchToDefaultContent(){
        _instanceDriver.switchTo().defaultContent();
    }

    /**
     * Select a frame by its name or ID. Frames located by matching name attributes are always given
     * precedence over those matched by ID.
     * @param frame Name or Id of the frame
     */
    public void switchToFrame(String frame){
        _instanceDriver.switchTo().frame(frame);
    }

    /**
     * Select a frame by its (zero-based) index. Selecting a frame by index is equivalent to the JS
     * expression window.frames[index] where "window" is the DOM window represented by the current
     * context. Once the frame has been selected, all subsequent calls on the WebDriver interface are
     * made to that frame.
     * @param frameIndex Index of frame begins in zero
     */
    public void switchToFrame(int frameIndex){
        _instanceDriver.switchTo().frame(frameIndex);
    }

    /**
     * Select a frame using its previously located WebElement.
     * @param frame The frame element to switch to.
     */
    public void switchToFrame(WebElement frame){
        _instanceDriver.switchTo().frame(frame);
    }

    /**
     * Switches to the currently active modal dialog for this particular driver instance.
     * @return A handle to the dialog.
     */
    public Alert switchToAlert(){
        return _instanceDriver.switchTo().alert();
    }

    /**
     * Get time the driver should wait when searching for an element if it is not immediately present.
     * @return Time to wait
     */
    public int getImplicitWaitSeconds(){
        return _implicitWaitSeconds;
    }

    /**
     * Specifies the amount of time the driver should wait when searching for an element if it is not immediately
     * present.
     * @param seconds The amount of time to wait.
     */
    public void setImplicitWait(int seconds){
        _implicitWaitSeconds = seconds;

        _instanceDriver.manage().timeouts().implicitlyWait(_implicitWaitSeconds, TimeUnit.SECONDS);
    }

    /**
     * Turn off the time the driver wait when searching for an element.
     */
    public void turnOffImplicitWaits() {
        _instanceDriver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
    }

    /**
     * Turn on the time the driver wait when searching for an element.
     */
    public void turnOnImplicitWaits() {
        _instanceDriver.manage().timeouts().implicitlyWait(_implicitWaitSeconds, TimeUnit.SECONDS);
    }

    /**
     * Find the first WebElement using the given method.
     * @param element WebElement within which the search will
     * @return The first matching element on the current context.
     * @throws Exception
     */
    public WebElement getParent(WebElement element) throws Exception{
        return element.findElement(By.xpath(".."));
    }

    /**
     * The driver should wait searching for an element
     * @param elementLocator The locating mechanism
     * @param timeoutSeconds Time in seconds
     * @return Whether or not the element is displayed
     * @throws Exception
     */
    public boolean waitForElement(By elementLocator, long timeoutSeconds) throws Exception{

        final By elem = elementLocator;

        WebElement myDynamicElement = (new WebDriverWait(_instanceDriver, timeoutSeconds))
                .until(new ExpectedCondition<WebElement>(){
                           @Override
                           public WebElement apply(WebDriver d) {
                               return d.findElement(elem);
                           }
                       }

                );

        return true;
    }

    /**
     * The driver should wait searching for an element
     * @param fromWebElement Web element container
     * @param elementLocator The locating mechanism
     * @param timeoutSeconds Time in seconds
     * @return Whether or not the element is displayed
     * @throws Exception
     */
    public boolean waitForElement(final WebElement fromWebElement, By elementLocator, long timeoutSeconds) throws Exception{

        final By elem = elementLocator;

        WebElement myDynamicElement = (new WebDriverWait(_instanceDriver, timeoutSeconds))
                .until(new ExpectedCondition<WebElement>(){
                           @Override
                           public WebElement apply(WebDriver d) {
                               return fromWebElement.findElement(elem);
                           }
                       }

                );

        return true;
    }

    /**
     * Wait for page state to be completed
     * @param timeoutSeconds seconds to wait for page to change status to completed
     * @throws Exception
     */
    public void waitForDocumentCompleted(long timeoutSeconds) throws Exception{

        ExpectedCondition<Boolean> expectation = new
                ExpectedCondition<Boolean>() {
                    public Boolean apply(WebDriver driver) {
                        return ((JavascriptExecutor)driver).executeScript("return document.readyState").equals("complete");
                    }
                };

        Wait<WebDriver> wait = new WebDriverWait(_instanceDriver,timeoutSeconds);
        try {
            wait.until(expectation);
        } catch(Throwable error) {
            throw new Exception("Page not completed.");
            //assertFalse("Timeout waiting for Page Load Request to complete.",true);
        }
    }

    /**
     * Wait for text change in WebElement
     * @param targetElement WebElement with text to change
     * @param oldText Old Text in WebElement
     * @param timeoutSeconds seconds to wait for text to change
     * @throws Exception
     */
    public void waitForTextNotEqual(final WebElement targetElement, final String oldText, long timeoutSeconds) throws Exception{
        (new WebDriverWait(_instanceDriver, timeoutSeconds)).until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver d) {
                if(!targetElement.getText().equals(oldText)){
                    return true;
                }else { return false; }
            }
        });
    }

    /**
     * Wait for Element to be clickable
     * @param searchCriteria The locating mechanism
     * @param timeoutSeconds Seconds to wait
     * @throws Exception
     */
    public void waitForElementToBeClickable(By searchCriteria, long timeoutSeconds) throws Exception{

        WebDriverWait wait = new WebDriverWait(_instanceDriver, timeoutSeconds); // wait for timeoutSeconds
        wait.until(ExpectedConditions.elementToBeClickable(searchCriteria));
    }

    /**
     * Wait for checking if the given text is present in the element that matches the given locator.
     * @param searchCriteria The locating mechanism
     * @param textToBePresent Text to be present
     * @param timeoutSeconds Seconds to wait
     * @throws Exception
     */
    public void waitForTextToBePresent(By searchCriteria, String textToBePresent, long timeoutSeconds) throws Exception{

        WebDriverWait wait = new WebDriverWait(_instanceDriver, timeoutSeconds); // wait for timeoutSeconds
        wait.until(ExpectedConditions.textToBePresentInElement(searchCriteria, textToBePresent));
    }

    /**
     * Get all elements brothers that precede to WebElement
     * @param currentElement Web element parent
     * @return A List of all WebElements, or an empty List if nothing matches.
     */
    public List<WebElement> getPreviousSimblingElements(WebElement currentElement){
        List<WebElement> resultElements = currentElement.findElements(By.xpath("preceding-sibling"));

        return resultElements;
    }

    /**
     * Get the javascript executor
     * @return javascript executor
     */
    public JavascriptExecutor getJavascriptExecutor(){
        //((JavascriptExecutor)browser.getInstanceDriver()).executeScript("arguments[0].value=arguments[1]", elem, fieldData[i][j].fieldValue);
        return _javascriptExecutor;
    }

    /**
     * Causes the currently executing thread to sleep (temporarily cease execution) for the specified number of
     * milliseconds, subject to the precision and accuracy of system timers and schedulers. The thread does not lose
     * ownership of any monitors.
     * @param timeMilliSeconds The length of time to sleep in milliseconds
     * @throws Exception
     */
    public void sleep(long timeMilliSeconds) throws Exception {
        Thread.sleep(timeMilliSeconds);
    }
}
