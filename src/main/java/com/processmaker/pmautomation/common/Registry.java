package com.processmaker.pmautomation.common;

import java.util.HashMap;
import java.util.Map;

public class Registry {
    private static Registry ourInstance = new Registry();
    private final Map registry = new HashMap();

    public static Registry getInstance() {
        return ourInstance;
    }

    private Registry() {
    }

    /**
     * Get Reference of stored object in registry identified by the key
     * @param key the identifier of the object
     * @return return the found object
     */
    public Object getReference(
            final Object key) {
        Object result = null;
        if (isRegistered(key)) {
            result = registry.get(key);
        }
        return result;
    }

    /**
     * Get status of Object key
     * @param key The identifier of the object.
     * @return True if Object is set in registry.
     */
    private boolean isRegistered(Object key) {
        return registry.containsKey(key);
    }

    /**
     * Register the specified object (value) int he registry identified by the specified key
     * @param key The identifier of the object.
     * @param value The value - object to be stored in the registry
     */
    public synchronized void register(
            Object key, Object value) {
        registry.put(key, value);
    }

    /**
     * Unregister the specified object from registry.
     * @param key the identifier of the object -value to unregister
     */
    public synchronized void unregister(
            Object key) {
        registry.remove(key);

    }
}
