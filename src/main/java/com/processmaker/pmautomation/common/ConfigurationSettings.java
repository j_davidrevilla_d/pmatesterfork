package com.processmaker.pmautomation.common;

import java.io.*;
import java.util.Properties;

public class ConfigurationSettings {
    private static ConfigurationSettings INSTANCE = null;
    private Properties applicationProperties;
    private String appConfigurationFile;

    /**
     * Constructor of ConfigurationSettings that read documents with configurations.
     * @param defaultConfFile File with parameters for run test in Browser and Operating System.
     * @param applicationConfFile File with parameters for the application for test.
     * @throws FileNotFoundException File not found check the route.
     * @throws IOException Error to read files.
     */
    private ConfigurationSettings(String defaultConfFile, String applicationConfFile) throws FileNotFoundException, IOException{
        FileInputStream propertiesFile = new FileInputStream(defaultConfFile);
        Properties defaultProperties = new Properties();

        this.appConfigurationFile = applicationConfFile;
        defaultProperties.load(propertiesFile);
        propertiesFile.close();

        this.applicationProperties = new Properties(defaultProperties);
        propertiesFile = new FileInputStream(this.appConfigurationFile);
        this.applicationProperties.load(propertiesFile);
        propertiesFile.close();
    }

    /**
     * Instance ConfigurationSettings without create an object.
     * @param defaultConfFile File with parameters for run test in Browser and Operating System.
     * @param applicationConfFile File with parameters for the application for test.
     * @throws FileNotFoundException File not found check the route.
     * @throws IOException Error to read files.
     */
    private static void createInstance(String defaultConfFile, String applicationConfFile) throws FileNotFoundException, IOException{
        if(INSTANCE == null)
            INSTANCE = new ConfigurationSettings(defaultConfFile, applicationConfFile);
    }

    /**
     * Create an ConfigurationSettings object with the files "default.conf" and "app.conf"
     * @return ConfigurationSetting object
     * @throws FileNotFoundException File not found check the route.
     * @throws IOException Error to read files.
     */
    public static ConfigurationSettings getInstance() throws FileNotFoundException, IOException{
        File f = new File("default.conf");
        File fa = new File("app.conf");
        if(f.exists() && fa.exists()) {
            ConfigurationSettings.getInstance("default.conf", "app.conf");
        }else{
            f = new File("." +  File.separator +"src"+ File.separator + "main"+ File.separator + "default.conf");
            fa = new File("." + File.separator +"src"+ File.separator +"main"+ File.separator + "app.conf");
            if(f.exists() && fa.exists()) {
                ConfigurationSettings.getInstance("." + File.separator +"src"+ File.separator + "main"+ File.separator + "default.conf", "." + File.separator +"src"+ File.separator + "main"+ File.separator + "app.conf");
            }
        }
        return INSTANCE;
    }

    /**
     * Create an ConfigurationSettings object specifying the files.
     * @param defaultConfFile File with parameters for run test in Browser and Operating System.
     * @param applicationConfFile File with parameters for the application for test.
     * @return ConfigurationSetting object.
     * @throws FileNotFoundException File not found check the route.
     * @throws IOException Error to read files.
     */
    public static ConfigurationSettings getInstance(String defaultConfFile, String applicationConfFile) throws FileNotFoundException, IOException{
        ConfigurationSettings.createInstance(defaultConfFile, applicationConfFile);
        return INSTANCE;
    }

    /**
     * Get the parameter from Application configuration (app.conf).
     * @param key Key to the application configuration file.
     * @return Value corresponding to the key.
     */
    public String getSetting(String key){
        return this.applicationProperties.getProperty(key);
    }

    /**
     * Set setting with key and value in the properties of Application
     * @param key Key Name
     * @param value Value for the key
     * @throws IOException
     */
    public void setSetting(String key, String value) throws IOException{
        this.setSetting(key, value, null);
    }

    /**
     * Set setting with key, value and comment in the properties of Application
     * @param key Key Name
     * @param value Value for the key
     * @param comment Comment for the key
     * @throws IOException
     */
    public void setSetting(String key, String value, String comment) throws IOException{
        FileOutputStream fos = new FileOutputStream(this.appConfigurationFile);
        this.applicationProperties.setProperty(key, value);
        this.applicationProperties.store(fos, comment);
        fos.close();
    }
}
