package com.processmaker.pmautomation.common;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

public class GetElement {

    /**
     * Gets an element using the Xpath after waiting for this element to be displayed
     * @param driver
     * @param xpathValue
     * @param waitForElement
     * @return WebElement
     * @throws Exception
     */
    public static WebElement getElementByXpath(WebDriver driver, String xpathValue, boolean waitForElement) throws Exception {
        if (waitForElement) {
            WaitTool.waitForElement(driver, By.xpath(xpathValue), 20);
        }
        WebElement auxListElement = driver.findElement(By.xpath(xpathValue));
        if(auxListElement == null){
            throw new Exception("Element: "+xpathValue+" not found.");
        }
        return auxListElement;
    }

    /**
     * Gets an Element using the Xpath
     * @param driver
     * @param xpathValue
     * @return
     * @throws Exception
     */
    public static WebElement getElementByXpath(WebDriver driver, String xpathValue) throws Exception {
        WebElement auxListElement = driver.findElement(By.xpath(xpathValue));
        if(auxListElement == null){
            throw new Exception("Element: "+xpathValue+" not found.");
        }
        return auxListElement;
    }

    /**
     * Gets a group of elements using the Xpath after waiting for these element to be displayed
     * @param driver
     * @param xpathValue
     * @param index
     * @param waitForElement
     * @return
     * @throws Exception
     */
    public static WebElement getElementsByXpath(WebDriver driver, String xpathValue, int index, boolean waitForElement) throws Exception{
        if(waitForElement) {
            WaitTool.waitForElement(driver, By.xpath(xpathValue), 20);
        }
        WebElement auxListElement;
        List<WebElement> auxListElements = driver.findElements(By.xpath(xpathValue));
        if(auxListElements.size() > 0){
            auxListElement = auxListElements.get(index);
        }else{
            throw new Exception("Element: "+xpathValue+" with index "+index+" not found.");
        }
        return auxListElement;
    }

    /**
     * Gets a group of elements using the Xpath
     * @param driver
     * @param xpathValue
     * @param index
     * @return
     * @throws Exception
     */
    public static WebElement getElementsByXpath(WebDriver driver, String xpathValue, int index) throws Exception{
        WebElement auxListElement;
        List<WebElement> auxListElements = driver.findElements(By.xpath(xpathValue));
        if(auxListElements.size() > 0){
            auxListElement = auxListElements.get(index);
        }else{
            throw new Exception("Element: "+xpathValue+" with index "+index+" not found.");
        }
        return auxListElement;
    }
}
