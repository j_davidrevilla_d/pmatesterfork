package com.processmaker.pmautomation.pages;

import com.processmaker.pmautomation.common.BrowserInstance;
import com.processmaker.pmautomation.common.GetElement;
import com.processmaker.pmautomation.common.Logger;
import com.processmaker.pmautomation.common.WaitTool;
//import com.processmaker.pmautomation.pages.BPMNDesigner.DesignerProcessList;
import com.processmaker.pmautomation.pages.PMPage;
//import com.processmaker.pmautomation.pages.UserProfile;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

public class Main extends PMPage {
    WebElement headerSection = null;
    WebElement logoutSection = null;
    WebElement logoutLink = null;
    WebElement userProfileLink = null;
    WebElement menuSection = null;
    WebElement homeMenu = null;
    WebElement designerMenu = null;
    WebElement dashboardMenu = null;
    WebElement adminMenu = null;
    List<WebElement> auxListElements = null;

    public Main(BrowserInstance browserInstance) throws Exception {
        super(browserInstance);

        verifyPage();
    }

    @Override
    public void verifyPage() throws Exception {
        browser.switchToDefaultContent();
        //headerSection = browser.getInstanceDriver().findElement(By.id("pm_header"));
        //****headerSection = GetElement.getElementByXpath(webDriver, "testing", true);****
        WaitTool.waitForElement(webDriver,By.id("pm_header"),10);
        auxListElements = webDriver.findElements(By.id("pm_header"));
        if(auxListElements.size() > 0){
            headerSection = auxListElements.get(0);
        }else{
            throw new Exception("PM header not found.");
        }
        //logoutSection = headerSection.findElement(By.className("logout"));
        WaitTool.waitForElement(webDriver,By.cssSelector(".logout"),15);
        auxListElements = webDriver.findElements(By.cssSelector(".logout"));
        if(auxListElements.size() > 0){
            logoutSection = auxListElements.get(0);
        }else{
            throw new Exception("Logout section not found.");
        }
        //userProfileLink = logoutSection.findElement(By.cssSelector("small label.textBlue a"));
        WaitTool.waitForElement(webDriver,By.cssSelector("small label.textBlue a"),10);
        auxListElements = logoutSection.findElements(By.cssSelector("small label.textBlue a"));
        if(auxListElements.size() > 0){
            userProfileLink = auxListElements.get(0);
        }else{
            throw new Exception("User profile link not found.");
        }
        //logoutLink = logoutSection.findElement(By.linkText("Logout"));
        WaitTool.waitForElement(webDriver,By.linkText("Logout"),10);
        auxListElements = logoutSection.findElements(By.linkText("Logout"));
        if(auxListElements.size() > 0){
            logoutLink = auxListElements.get(0);
        }else{
            throw new Exception("Logout link not found.");
        }
        //menuSection = browser.getInstanceDriver().findElement(By.className("mainMenuBG"));
        WaitTool.waitForElement(webDriver,By.className("mainMenuBG"),10);
        auxListElements = webDriver.findElements(By.className("mainMenuBG"));
        if(auxListElements.size() > 0){
            menuSection = auxListElements.get(0);
        }else{
            throw new Exception("Menu section not found.");
        }

        //the following menu options are display in base to user permissions:
        //if the option is not available is set to null
        browser.turnOffImplicitWaits();
        //homeMenu = menuSection.findElement(By.id("CASES"));
        auxListElements = menuSection.findElements(By.cssSelector("#CASES a"));
        if(auxListElements.size() > 0){
            homeMenu = auxListElements.get(0);
        }
        //designerMenu = menuSection.findElement(By.id("PROCESSES"));
        auxListElements = menuSection.findElements(By.cssSelector("#PROCESSES a"));
        if(auxListElements.size() > 0){
            designerMenu = auxListElements.get(0);
        }
        //dashboardMenu = menuSection.findElement(By.id("DASHBOARD"));
        auxListElements = menuSection.findElements(By.cssSelector("#DASHBOARD a"));
        if(auxListElements.size() > 0){
            dashboardMenu = auxListElements.get(0);
        }
        //adminMenu = menuSection.findElement(By.id("SETUP"));
        auxListElements = menuSection.findElements(By.cssSelector("#SETUP a"));
        if(auxListElements.size() > 0){
            adminMenu = auxListElements.get(0);
        }
        browser.turnOnImplicitWaits();

        Logger.addLog("Main page verified.");
    }

    /**
     * Click in the option Home to the Menu.
     * @return Home object with represent Home Page
     * @throws Exception
     */
    public void goHome() throws Exception{
        browser.switchToDefaultContent();
        if(homeMenu == null){
            throw new Exception("Home Menu not found.");
        }
        this.homeMenu.click();
        //return new Home(browser);
    }

    /**
     * Click in the option Designer to the Menu
     * @return DesignerProcessList Object
     * @throws Exception
     */
    public void goDesigner() throws Exception{
        browser.switchToDefaultContent();
        if(designerMenu == null){
            throw new Exception("Designer Menu not found.");
        }
        this.designerMenu.click();
        //create designer page
        //return new DesignerProcessList(browser);
    }

    /**
     * Click in the option Dashborads to the Menu
     * @throws Exception
     */
    public void goDashboards() throws Exception{
        browser.switchToDefaultContent();
        if(dashboardMenu == null){
            throw new Exception("Dashboard Menu not found.");
        }
        this.dashboardMenu.click();
    }

    /**
     * Click in the option Admin to the Menu
     * @throws Exception
     */
    public void goAdmin() throws Exception{
        browser.switchToDefaultContent();
        if(adminMenu == null){
            throw new Exception("Admin Menu not found.");
        }
        adminMenu.click();
    }

    /**
     * Click in logout link
     * @throws Exception
     */
    public void logout() throws Exception{
        browser.switchToDefaultContent();
        //logoutSection.findElements(By.linkText("Logout")).get(0).click();

        auxListElements = webDriver.findElements(By.linkText("Logout"));
        if(auxListElements.size() > 0){
            logoutLink = auxListElements.get(0);
        }else{
            throw new Exception("Logout link not found.");
        }

        this.logoutLink.click();
    }

    /**
     * Click in link with username to redirect to profile user
     * @throws Exception
     */
    public void profile() throws Exception{
        browser.switchToDefaultContent();
        userProfileLink.click();
        //return new UserProfile(browser);
    }

    /**
     * Refresh browser
     * @throws Exception
     */
    public void refreshPage()throws Exception{
        browser.refresh();
    }
}