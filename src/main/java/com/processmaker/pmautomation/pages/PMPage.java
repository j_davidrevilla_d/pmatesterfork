package com.processmaker.pmautomation.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import com.processmaker.pmautomation.common.BrowserInstance;
import com.processmaker.pmautomation.common.WaitTool;

public abstract class PMPage extends Page {
    public int waitTimeParameter = 30;
    public PMPage(BrowserInstance browser) throws Exception {
        super(browser);
    }

    /**
     * Frame of the designer
     * @throws Exception
     */
    public void switchToMainFrame() throws Exception {
        browser.switchToDefaultContent();
        WaitTool.waitForFrameToBeAvailableAndSwitchToIt(browser.getInstanceDriver(), "frameMain", waitTimeParameter);
        //browser.switchToFrame("frameMain");
    }

    /**
     * Frame of the with Tiny Editor
     */
    public void switchToTinyEditorFrame(){
        WaitTool.waitForFrameToBeAvailableAndSwitchToIt(browser.getInstanceDriver(), "tinyeditor_ifr", waitTimeParameter);
    }

    /**
     * Frame of the Report Tables
     * @throws Exception
     */
    public void switchToReportTableFrame() throws Exception {
        this.switchToMainFrame();
        WaitTool.waitForFrameToBeAvailableAndSwitchToIt(browser.getInstanceDriver(), "PMIframeWindow", waitTimeParameter);
    }

    /**
     * Frame of the Fields Handler
     * @throws Exception
     */
    public void switchToFieldsHandlerFrame() throws Exception{
        this.switchToMainFrame();
        WaitTool.waitForFrameToBeAvailableAndSwitchToIt(browser.getInstanceDriver(), "dynaframe", waitTimeParameter);
    }

    /**
     * Frame of the JSEditor
     * @param frame WebElement Frame
     * @throws Exception
     */
    public void switchToJSEditorFrame(WebElement frame) throws Exception{
        this.switchToMainFrame();
        browser.switchToFrame(frame);
    }

    /**
     * Frame of the Dynaform Dynamic
     * @param frame WebElement Frame
     * @throws Exception
     */
    public void switchToDynaformDynamicFrame(WebElement frame) throws Exception{
        browser.switchToDefaultContent();
        WaitTool.waitForFrameToBeAvailableAndSwitchToIt(browser.getInstanceDriver(),"casesFrame", waitTimeParameter);
        WaitTool.waitForFrameToBeAvailableAndSwitchToIt(browser.getInstanceDriver(), "casesSubFrame", waitTimeParameter);
        browser.switchToFrame(frame);
    }

    /**
     * Frame of the Admin option
     * @throws Exception
     */
    public void switchToAdminFrame() throws Exception{
        browser.switchToDefaultContent();
        WaitTool.waitForFrameToBeAvailableAndSwitchToIt(browser.getInstanceDriver(),"adminFrame", waitTimeParameter);
        //browser.switchToFrame("adminFrame");
    }

    /**
     * Frame of the Home used also in Dashboards
     * @throws Exception
     */
    public void switchToCasesFrame() throws Exception {
        browser.switchToDefaultContent();
        WaitTool.waitForFrameToBeAvailableAndSwitchToIt(browser.getInstanceDriver(),"casesFrame", waitTimeParameter);
        //browser.switchToFrame("casesFrame");
    }

    /**
     * Frame of the open case
     * @throws Exception
     */
    public void switchToCasesSubFrameOpenCase() throws Exception {
        browser.switchToDefaultContent();
        WaitTool.waitForFrameToBeAvailableAndSwitchToIt(browser.getInstanceDriver(),"casesFrame", waitTimeParameter);
        //browser.switchToFrame("casesFrame");
        WaitTool.waitForFrameToBeAvailableAndSwitchToIt(browser.getInstanceDriver(),"casesSubFrame", waitTimeParameter);
        WaitTool.waitForElement(browser.getInstanceDriver(), By.id("navPanel"), waitTimeParameter);
        //browser.switchToFrame("casesSubFrame");
    }
    /**
     * Frame of the open case
     * @throws Exception
     */
    public void switchToCasesSubFrame() throws Exception {
        browser.switchToDefaultContent();
        WaitTool.waitForFrameToBeAvailableAndSwitchToIt(browser.getInstanceDriver(),"casesFrame", waitTimeParameter);
        WaitTool.waitForFrameToBeAvailableAndSwitchToIt(browser.getInstanceDriver(),"casesSubFrame", waitTimeParameter);
        //WaitTool.waitForElement(browser.getInstanceDriver(), By.id("casesSubFrame"), 15);
    }

    /**
     * Frame of the open case
     * @throws Exception
     */
    public void switchToMessageHistoryFrame() throws Exception {
        browser.switchToDefaultContent();
        WaitTool.waitForFrameToBeAvailableAndSwitchToIt(browser.getInstanceDriver(),"casesFrame", waitTimeParameter);
        WaitTool.waitForFrameToBeAvailableAndSwitchToIt(browser.getInstanceDriver(),"casesSubFrame", waitTimeParameter);
        WaitTool.waitForFrameToBeAvailableAndSwitchToIt(browser.getInstanceDriver(),"messageHistoryFrame", waitTimeParameter);
    }

    /**
     * Frame of the grid of cases
     * @throws Exception
     */
    public void switchToCasesSubFrameCasesGrid() throws Exception {
        browser.switchToDefaultContent();
        WaitTool.waitForFrameToBeAvailableAndSwitchToIt(browser.getInstanceDriver(),"casesFrame", waitTimeParameter);
        //browser.switchToFrame("casesFrame");
        WaitTool.waitForFrameToBeAvailableAndSwitchToIt(browser.getInstanceDriver(),"casesSubFrame", waitTimeParameter);
        WaitTool.waitForElement(browser.getInstanceDriver(), By.id("casesGrid"), waitTimeParameter);
        //browser.switchToFrame("casesSubFrame");
    }

    /**
     * Frame of the dynaformHistoryFrame
     * @throws Exception
     */
    public void switchToDynaformHistoryFrame() throws Exception {
        browser.switchToDefaultContent();
        WaitTool.waitForFrameToBeAvailableAndSwitchToIt(browser.getInstanceDriver(),"casesFrame", waitTimeParameter);
        WaitTool.waitForFrameToBeAvailableAndSwitchToIt(browser.getInstanceDriver(),"casesSubFrame", waitTimeParameter);
        WaitTool.waitForFrameToBeAvailableAndSwitchToIt(browser.getInstanceDriver(),"dynaformHistoryFrame", waitTimeParameter);
    }

    /**
     * Frame of the open case without toolbar of case
     * @throws Exception
     */
    public void switchToOpenCaseFrame() throws Exception {
        browser.switchToDefaultContent();
        WaitTool.waitForFrameToBeAvailableAndSwitchToIt(browser.getInstanceDriver(),"casesFrame", waitTimeParameter);
        //browser.switchToFrame("casesFrame");
        WaitTool.waitForFrameToBeAvailableAndSwitchToIt(browser.getInstanceDriver(),"casesSubFrame", waitTimeParameter);
        WaitTool.waitForElement(browser.getInstanceDriver(), By.id("navPanel"), waitTimeParameter);
        //browser.switchToFrame("casesSubFrame");
        WaitTool.waitForFrameToBeAvailableAndSwitchToIt(browser.getInstanceDriver(),"openCaseFrame", waitTimeParameter);
        WaitTool.waitForElement(browser.getInstanceDriver(), By.cssSelector("body > table"), waitTimeParameter);
        //browser.switchToFrame("openCaseFrame");
    }

    /**
     * Frame of treePanel and grid for documents
     * @throws Exception
     */
    public void switchToCasesSubFrameCasesTreeAndGrid() throws Exception {
        browser.switchToDefaultContent();
        WaitTool.waitForFrameToBeAvailableAndSwitchToIt(browser.getInstanceDriver(),"casesFrame", waitTimeParameter);
        WaitTool.waitForFrameToBeAvailableAndSwitchToIt(browser.getInstanceDriver(),"casesSubFrame", waitTimeParameter);
        WaitTool.waitForElement(browser.getInstanceDriver(), By.id("documents"), waitTimeParameter);
    }

    /**
     * Frame of gridPanel for Case History of the Case Information Menu
     */
    public void switchToCasesSubFrameCasesHistoryGrid() throws Exception {
        browser.switchToDefaultContent();
        WaitTool.waitForFrameToBeAvailableAndSwitchToIt(browser.getInstanceDriver(),"casesFrame", waitTimeParameter);
        WaitTool.waitForFrameToBeAvailableAndSwitchToIt(browser.getInstanceDriver(),"casesSubFrame", waitTimeParameter);
        WaitTool.waitForFrameToBeAvailableAndSwitchToIt(browser.getInstanceDriver(),"caseHistoryFrame", waitTimeParameter);
        WaitTool.waitForElement(browser.getInstanceDriver(), By.id("processesGrid"), waitTimeParameter);
    }

    /**
     * Frame for Process Map of the Case Information Menu
     */
    public void switchToCasesSubFrameProcessMap() throws Exception {
        browser.switchToDefaultContent();
        WaitTool.waitForFrameToBeAvailableAndSwitchToIt(browser.getInstanceDriver(),"casesFrame", waitTimeParameter);
        WaitTool.waitForFrameToBeAvailableAndSwitchToIt(browser.getInstanceDriver(),"casesSubFrame", waitTimeParameter);
        WaitTool.waitForFrameToBeAvailableAndSwitchToIt(browser.getInstanceDriver(),"processMapFrame", waitTimeParameter);
        WaitTool.waitForElement(browser.getInstanceDriver(), By.className("content"), waitTimeParameter);
    }

    /**
     * Frame for Change Log from Case Histoy
     */
    public void switchToChangeLogTab(int indexFrame) throws Exception {
        browser.switchToDefaultContent();
        WaitTool.waitForFrameToBeAvailableAndSwitchToIt(browser.getInstanceDriver(), "casesFrame", waitTimeParameter);
        WaitTool.waitForFrameToBeAvailableAndSwitchToIt(browser.getInstanceDriver(), "casesSubFrame", waitTimeParameter);
        WaitTool.waitForFrameToBeAvailableAndSwitchToIt(browser.getInstanceDriver(),By.xpath("/html/body/div/div/div[2]/div[2]/div/div/div/div[2]/div/div["+(3+indexFrame)+"]/div/div/iframe"),waitTimeParameter);
    }

    /**
     * Frame for Dynaform View From Histoy
     */
    public void switchToDynaformViewFromHistory(int indexFrame) throws Exception {
        browser.switchToDefaultContent();
        WaitTool.waitForFrameToBeAvailableAndSwitchToIt(browser.getInstanceDriver(), "casesFrame", waitTimeParameter);
        WaitTool.waitForFrameToBeAvailableAndSwitchToIt(browser.getInstanceDriver(), "casesSubFrame", waitTimeParameter);
        WaitTool.waitForFrameToBeAvailableAndSwitchToIt(browser.getInstanceDriver(),By.xpath("/html/body/div/div/div[2]/div[2]/div/div/div/div[2]/div/div["+(3+indexFrame)+"]/div/div/iframe"),waitTimeParameter);
    }

    /**
     * Frame for Generated Documents from Information Menu
     */
    public void switchToGeneratedDocumentsFromInformationMenu() throws Exception {
        browser.switchToDefaultContent();
        WaitTool.waitForFrameToBeAvailableAndSwitchToIt(browser.getInstanceDriver(), "casesFrame", waitTimeParameter);
        WaitTool.waitForFrameToBeAvailableAndSwitchToIt(browser.getInstanceDriver(), "casesSubFrame", waitTimeParameter);
        WaitTool.waitForFrameToBeAvailableAndSwitchToIt(browser.getInstanceDriver(), "generatedDocumentsFrame", waitTimeParameter);
    }

    /**
     * Frame for Generated Documents from Case Summary
     */
    public void switchToGeneratedDocumentsFromCaseSummary() throws Exception {
        browser.switchToDefaultContent();
        WaitTool.waitForFrameToBeAvailableAndSwitchToIt(browser.getInstanceDriver(), "casesFrame", waitTimeParameter);
        WaitTool.waitForFrameToBeAvailableAndSwitchToIt(browser.getInstanceDriver(), "casesSubFrame", waitTimeParameter);
        WaitTool.waitForFrameToBeAvailableAndSwitchToIt(browser.getInstanceDriver(), "summaryIFrame", waitTimeParameter);
    }

    /**
     * Frame for Generated Documents from Information Menu
     */
    public void switchToUploadedDocumentsFromInformationMenu() throws Exception {
        browser.switchToDefaultContent();
        WaitTool.waitForFrameToBeAvailableAndSwitchToIt(browser.getInstanceDriver(), "casesFrame", waitTimeParameter);
        WaitTool.waitForFrameToBeAvailableAndSwitchToIt(browser.getInstanceDriver(), "casesSubFrame", waitTimeParameter);
        WaitTool.waitForFrameToBeAvailableAndSwitchToIt(browser.getInstanceDriver(), "uploadedDocumentsFrame", waitTimeParameter);
    }

    /**
     * Frame for Generated Documents from Case Summary
     */
    public void switchToUploadedDocumentsFromCaseSummary() throws Exception {
        browser.switchToDefaultContent();
        WaitTool.waitForFrameToBeAvailableAndSwitchToIt(browser.getInstanceDriver(), "casesFrame", waitTimeParameter);
        WaitTool.waitForFrameToBeAvailableAndSwitchToIt(browser.getInstanceDriver(), "casesSubFrame", waitTimeParameter);
        WaitTool.waitForFrameToBeAvailableAndSwitchToIt(browser.getInstanceDriver(), "summaryIFrame", waitTimeParameter);
    }
    public void switchToIFrame() throws Exception {
        switchToMainFrame();
        WaitTool.waitForFrameToBeAvailableAndSwitchToIt(browser.getInstanceDriver(), "iframe", 10);
    }
}


