package com.processmaker.pmautomation.pages;

import com.processmaker.pmautomation.common.BrowserInstance;
import com.processmaker.pmautomation.common.ConfigurationSettings;

import java.io.IOException;

public class Pages {
    protected BrowserInstance _browserInstance;

    public Pages(BrowserInstance browserInstance){
        _browserInstance = browserInstance;
    }

    /**
     * Go to url defined in default.conf with key server.url
     * @throws IOException
     */
    public void gotoDefaultUrl() throws IOException {
        String url;
        //default url
        url = ConfigurationSettings.getInstance().getSetting("server.url");

        _browserInstance.gotoUrl(url);
    }


    /**
     * Create a Login page Object
     * @see com.processmaker.pmautomation.pages.Login Login Page
     * @return Login Page Object
     * @throws Exception
     */
    public Login Login() throws Exception{

        return new Login(_browserInstance);
    }

    /**
     * Create a Main page Object
     * @see com.processmaker.pmautomation.pages.Main Main Page
     * @return Main Page Object
     * @throws Exception
     */
    public Main Main() throws Exception{

        return new Main(_browserInstance);
    }

}
