package com.processmaker.pmautomation.pages.admin;

import com.processmaker.pmautomation.common.BrowserInstance;
import com.processmaker.pmautomation.pages.PMPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class Admin extends PMPage {


    public Admin(BrowserInstance browserInstance) throws Exception {
        super(browserInstance);

    }

    @Override
    public void verifyPage() throws Exception {
        //return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public void goToLogs() throws Exception{
        browser.switchToDefaultContent();
        browser.waitForElement(By.id("adminFrame"),120);
        browser.switchToFrame("adminFrame");
        WebElement we = webDriver.findElement(By.xpath("//*[@id='west-panel__logs']/a[2]"));
        we.click();
        //browser.switchToDefaultContent();
    }

    public void goToPlugins() throws Exception{
        browser.switchToDefaultContent();
        browser.waitForElement(By.id("adminFrame"),120);
        browser.switchToFrame("adminFrame");
        WebElement we = webDriver.findElement(By.xpath("//*[@id='west-panel__plugins']/a[2]"));
        we.click();
    }
   
    public void goToUsers() throws Exception{
        browser.switchToDefaultContent();
        browser.waitForElement(By.id("adminFrame"),120);
        browser.switchToFrame("adminFrame");
        WebElement we = webDriver.findElement(By.xpath("//*[@id='west-panel__users']/a[2]"));
        we.click();
    }
}