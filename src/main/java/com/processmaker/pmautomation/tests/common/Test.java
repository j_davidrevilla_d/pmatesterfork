package com.processmaker.pmautomation.tests.common;

import com.processmaker.pmautomation.common.BrowserInstance;
import com.processmaker.pmautomation.common.BrowserSettings;
import com.processmaker.pmautomation.common.Logger;
import com.processmaker.pmautomation.pages.Pages;


//@RunWith(value = Parameterized.class)
//@RunWith(value = Suite.class)
public abstract class Test {
    protected String browserName;
    protected BrowserInstance browserInstance;
    protected Pages pages;

    public Test() throws Exception {
        //get browser configuration from parameters
        //-Dbrowser.configuration=firefox
        String browserConfiguration = System.getProperty("browser.configuration");

        Logger.addLog("browserConfiguration ->:" + browserConfiguration);

        if(browserConfiguration == null){
            Logger.addLog("Browser Settings not detected, use default browser configuration.");
            browserConfiguration = "";
        }

        //read browser settings
        BrowserSettings browserSettings = BrowserInstance.getBrowserSettings(browserConfiguration);

        Logger.addLog("Create new browser instance. Mode:" + browserSettings.getBrowserMode() +
                " Name:" + browserSettings.getBrowserName() + " Platform:" + browserSettings.getBrowserPlatform() +
                " Version:" + browserSettings.getBrowserVersion() + " url:" + browserSettings.getRemoteServerUrl());
        browserInstance = new BrowserInstance(browserSettings);

        //initialize pages
        pages = new Pages(browserInstance);
    }

    public void goToUrl(String url){
        browserInstance.gotoUrl(url);
    }
}
