package com.processmaker.pmautomation;


import com.processmaker.pmautomation.common.Logger;
import com.processmaker.pmautomation.pages.Main;
import com.processmaker.pmautomation.tests.common.Test;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;

import java.io.FileNotFoundException;
import java.io.IOException;


public class App extends Test{
    public App() throws Exception{
        
    }
    @Before
    public void setup(){

    }

    @After
    public void cleanup(){
        browserInstance.quit();
    }

    @org.junit.Test
    public void testProcess() throws FileNotFoundException, IOException, Exception{
        pages.gotoDefaultUrl();
        Logger.addLog("Test OutputDocument with browserName:" + this.browserName);
        pages.Login().loginFocos("adolfo@processmaker.com","sample");
    }
}
